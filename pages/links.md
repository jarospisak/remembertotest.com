---
title: Testing Links
subtitle: Everything you need to know about testing links
layout: page
permalink: /links
---

What to check for:
- there are no broken links on the website. The easiest way to do a bulk check-up for links on a website is to use some sort of link scanner. There are several free scanners available which will tell you:
  - 404s
  - redirections
  - etc.


The best scanner out there is Xenu Link Sleuth (available only for Windows). It's so much better than any other alternatives that if you don't have windows, consider running it via Wine or virtual box http://home.snafu.de/tilman/xenulink.html

There is only so much automated link checking can verify. Make sure the links are pointing to correct pages, if they point to documents, the documents should be the correct ones

404s
You can easily identify all 404 pages on your site with some of the free tools. One of the best tools out there is Xenu Link Sleuth http://home.snafu.de/tilman/xenulink.html but it's available only on Windows. It's much better than any other alternatives so if you don't have a Windows machine, consider running it via Wine, VirtualBox or any other virtualization software.

If you are comfortable using terminal, you can use wget to crawl your site, check the links and save the results to a file. You can then either manually go through the file and check for the 404 pages or search the file with grep https://www.digitalocean.com/community/tutorials/how-to-find-broken-links-on-your-website-using-wget-on-debian-7.

https://moz.com/blog/xenu-link-sleuth-more-than-just-a-broken-links-finder

