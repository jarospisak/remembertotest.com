---
title: Testing Forms
subtitle: Forms can be tricky to test 
layout: page
permalink: /forms
---

Here should come some summary about testing forms, what to focus on etc.

### Boundary Testing

One of the most important things to test in forms is boundary values. Boundary tests use extreme values to test the input fields. For example, you could try inserting the following in the text input fields:

*   maximum allowed length
*   maximum allowed length + 1
*   minimum required length
*   minimum required length - 1
*   not allowed characters (!#€% etc.)
*   HTML tags
*   etc.

You get the idea. The aim is to discover and verify the boundaries of the form fields. Test that form accepts all the expected values and displays relevant error message for unexpected values.

### Inline Form Validation

It's considered good user experience to validate the form on the fly before submitting it. This is done with Javascript with an aim to provide instant feedback to the user.

Error message should tell user what are the valid input characters. It should be displayed close to the input field and the field should be highlighted (Figure 1).

![Inline form validation](/img/inline-form-validation.gif)
*Figure 1: On the fly inline validation provides instant feedback to the user.*

### Form Validation After Submit

If on the fly input validation is not available, submitting form should return user to the form with invalid fields highlighted. Each invalid field should also have a corresponding error message explaining why the inserted value is not valid and/or provide a list of valid values.

Your website should not rely solely on Javascript for the input validation. Some users might use the site with Javascript disabled. Test that you provide meaningful error messages even if Javascript is disabled.

### Submit Button

It's a recommended practice to disable submit button until all the required fields have been filled (Figure 2). It's also worth considering disabling the submit button right after the form has been submitted to prevent multiple submissions. Enable it only after the submission has been completed.

![Disabled submit button](/img/disabled-submit-button.gif)
*Figure 2: "Create My Account" button is enabled only after all fields are filled.*

### Form Usability

Have you ever filled in a form, clicked the submit button, got notified that you forgot to fill in some of the required fields and found out that some of the values you previously entered are now gone? When testing forms, pay special attention to these kind of usability issues. They might seem trivial but they can annoy users enough to leave your site.

### Form Navigation

More experienced users will want to navigate the form by using only the tab key. To test that, click on the first form field and press tab key. Does the next field get focused? If it's a text field, type something in and press tab. Focus should move on to the next input field. Focus should move on to the next input field.

When it comes to radio buttons and check boxes, it should be possible to make a selection with space bar.

When testing the form, remember to check that focus moves from one field to another in an orderly fashion. If you have a form with fields like title, first name, middle name and surname, the tab should move from field to field in logical order.

Also, it should be possible submit the form by simply pressing enter.

### Useful Tools For Testing Web Forms

*   Browser extensions to speed up filling forms: [Chrome](https://chrome.google.com/webstore/search/fill%20form?hl=en&_category=extensions) | [Firefox](https://addons.mozilla.org/en-US/firefox/search/?q=fill+form)
