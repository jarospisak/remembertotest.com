---
title: Redirection
subtitle: Some subtitle
layout: page
permalink: /redirection
---

* non-www to www redirection
* http to https redirection
* some older mobile browsers won't redirect